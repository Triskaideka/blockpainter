# Block Painter

The Block Painter is a web page that uses HTML, CSS, and JavaScript to display colored blocks according to the information you specify in the URL query.  When called with no query, it will present an editor.  It is written in less than one kiB of code and was originally hosted at the now-defunct [onekb.net](http://onekb.net/), but has since been moved to my [personal web site](https://triskaideka.net/onekb/blockpainter/).

This tool is not likely to have any practical application; it's just a demo to show what can be done with a small amount of code.

## How to Use

The query (i.e. the part of the URL after the question mark) is a semicolon-separated list of blocks, and each block is a comma-separated list of values in the format:

    color,left_offset,width,top_offset,height

The color should be a hexadecimal color code (three or six digits).  The other numbers should be usable as percentages (they may contain a decimal point).

To use [the editor](index.html), type legal values (as described above) in each of the five fields.  Click the `+` to add a set of fields for a new block, or click `x` to delete the corresponding block.  When finished, click the preview area to open your artwork at full size.

Here's a good way to start experimenting: enter `f90` in the color field and `49` in all of the other fields.  Then play around with the numbers and see what happens.

## Development

Developers interested in expanding upon the Block Painter should consult [blockpainter_symbols.txt](blockpainter_symbols.txt) to find out what all those one-letter variables mean.  (I minified the code by hand, so there is no "pre-minified" version to refer to.)

## Gallery

The "local" links should work if you're viewing this file on your local disk, but they won't if you're viewing it at GitLab.  If they don't work, use the "@ triskaideka.net" links.

### Flags

+ Poland
  + [reference @ Wikipedia](https://en.wikipedia.org/wiki/Flag_of_Poland)
  + paint it: [local](index.html?d4213d,0,100,50,50) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?d4213d,0,100,50,50)
+ Ukraine
  + [reference @ Wikipedia](https://en.wikipedia.org/wiki/Flag_of_Ukraine)
  + paint it: [local](index.html?0057b7,0,100,0,50;ffd700,0,100,50,50) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?0057b7,0,100,0,50;ffd700,0,100,50,50)
+ Ireland
  + [reference @ Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Flag_of_Ireland.svg)
  + paint it: [local](index.html?009b48,0,33,0,100;ff7900,67,33,0,100) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?009b48,0,33,0,100;ff7900,67,33,0,100)
+ Greece
  + [reference @ Wikipedia](https://en.wikipedia.org/wiki/Flag_of_Greece)
  + paint it: [local](index.html?005bae,0,100,0,100;fff,37,63,11.111,11.111;fff,37,63,33.333,11.111;fff,0,100,55.555,11.111;fff,0,100,77.777,11.111;fff,0,37,22.222,11.111;fff,15,7.67,0,55.555) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?005bae,0,100,0,100;fff,37,63,11.111,11.111;fff,37,63,33.333,11.111;fff,0,100,55.555,11.111;fff,0,100,77.777,11.111;fff,0,37,22.222,11.111;fff,15,7.67,0,55.555)
+ Norway
  + [reference @ Wikipedia](https://en.wikipedia.org/wiki/File:Flag_of_Norway_with_proportions.svg)
  + paint it: [local](index.html?ef2b2d,0,100,0,100;fff,27.27,18.18,0,100;fff,0,100,37.5,25;002868,31.82,9.09,0,100;002868,0,100,43.75,12.5) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?ef2b2d,0,100,0,100;fff,27.27,18.18,0,100;fff,0,100,37.5,25;002868,31.82,9.09,0,100;002868,0,100,43.75,12.5)

### Words

+ "trisk"
  + paint it: [local](index.html?bbb,0,100,0,100;6a5acd,8,5,50,50;6a5acd,3,20,60,5;6a5acd,13,10,95,5;6a5acd,18,5,88,7;6a5acd,26,5,57,43;6a5acd,31,11,60,5;6a5acd,37,5,65,5;6a5acd,45,5,63,37;6a5acd,45,5,52,5;6a5acd,53,5,60,22;6a5acd,53,5,88,7;6a5acd,58,14,60,5;6a5acd,58,9,77,5;6a5acd,67,5,77,18;6a5acd,53,19,95,5;6a5acd,67,5,65,6;6a5acd,75,5,35,65;6a5acd,80,15,68,5;6a5acd,90,5,73,27;6a5acd,80,4,67,1;6a5acd,80,5,66,1;6a5acd,81,5,65,1;6a5acd,82,5,64,1;6a5acd,83,5,63,1;6a5acd,84,5,62,1;6a5acd,85,5,61,1;6a5acd,86,5,60,1;6a5acd,87,5,59,1;6a5acd,88,5,58,1;6a5acd,89,5,57,1;6a5acd,90,5,56,1;6a5acd,91,4,55,1;6a5acd,92,3,54,1;6a5acd,93,2,53,1;6a5acd,94,1,52,1) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?bbb,0,100,0,100;6a5acd,8,5,50,50;6a5acd,3,20,60,5;6a5acd,13,10,95,5;6a5acd,18,5,88,7;6a5acd,26,5,57,43;6a5acd,31,11,60,5;6a5acd,37,5,65,5;6a5acd,45,5,63,37;6a5acd,45,5,52,5;6a5acd,53,5,60,22;6a5acd,53,5,88,7;6a5acd,58,14,60,5;6a5acd,58,9,77,5;6a5acd,67,5,77,18;6a5acd,53,19,95,5;6a5acd,67,5,65,6;6a5acd,75,5,35,65;6a5acd,80,15,68,5;6a5acd,90,5,73,27;6a5acd,80,4,67,1;6a5acd,80,5,66,1;6a5acd,81,5,65,1;6a5acd,82,5,64,1;6a5acd,83,5,63,1;6a5acd,84,5,62,1;6a5acd,85,5,61,1;6a5acd,86,5,60,1;6a5acd,87,5,59,1;6a5acd,88,5,58,1;6a5acd,89,5,57,1;6a5acd,90,5,56,1;6a5acd,91,4,55,1;6a5acd,92,3,54,1;6a5acd,93,2,53,1;6a5acd,94,1,52,1)
+ "peace" in multicolor
  + paint it: [local](index.html?00bfff,3,6,3,94;00bfff,9,22,3,10;00bfff,9,22,31,10;00bfff,25,6,13,18;d10000,38,6,3,94;d10000,56,6,3,94;d10000,44,12,3,10;d10000,44,12,45,10;3cb371,66,6,3,94;3cb371,72,21,3,10;3cb371,72,21,87,10;ffa500,12,6,45,52;ffa500,29,6,45,31;ffa500,18,11,45,10;ffa500,18,11,66,10;ffa500,18,17,87,10;9370d8,75,6,21,57;9370d8,91,6,21,34;9370d8,81,10,21,10;9370d8,81,10,45,10;9370d8,81,16,68,10) or [@triskaideka.net](https://triskaideka.net/onekb/blockpainter/?00bfff,3,6,3,94;00bfff,9,22,3,10;00bfff,9,22,31,10;00bfff,25,6,13,18;d10000,38,6,3,94;d10000,56,6,3,94;d10000,44,12,3,10;d10000,44,12,45,10;3cb371,66,6,3,94;3cb371,72,21,3,10;3cb371,72,21,87,10;ffa500,12,6,45,52;ffa500,29,6,45,31;ffa500,18,11,45,10;ffa500,18,11,66,10;ffa500,18,17,87,10;9370d8,75,6,21,57;9370d8,91,6,21,34;9370d8,81,10,21,10;9370d8,81,10,45,10;9370d8,81,16,68,10)
+ digital "future"
  + paint it: [local](index.html?000,0,100,0,100;fff,1,3,28,20;fff,5,4,26,3;fff,1,3,51,20;fff,5,4,48,3;fff,18,3,51,20;fff,18,3,28,20;fff,22,4,70,3;fff,27,3,28,20;fff,27,3,51,20;fff,35,3,51,20;fff,35,3,28,20;fff,39,4,70,3;fff,39,4,48,3;fff,52,3,51,20;fff,52,3,28,20;fff,61,3,28,20;fff,56,4,70,3;fff,61,3,51,20;fff,69,3,51,20;fff,69,3,28,20;fff,73,4,26,3;fff,86,3,28,20;fff,95,3,28,20;fff,90,4,26,3;fff,90,4,48,3;fff,90,4,70,3;fff,86,3,51,20) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?000,0,100,0,100;fff,1,3,28,20;fff,5,4,26,3;fff,1,3,51,20;fff,5,4,48,3;fff,18,3,51,20;fff,18,3,28,20;fff,22,4,70,3;fff,27,3,28,20;fff,27,3,51,20;fff,35,3,51,20;fff,35,3,28,20;fff,39,4,70,3;fff,39,4,48,3;fff,52,3,51,20;fff,52,3,28,20;fff,61,3,28,20;fff,56,4,70,3;fff,61,3,51,20;fff,69,3,51,20;fff,69,3,28,20;fff,73,4,26,3;fff,86,3,28,20;fff,95,3,28,20;fff,90,4,26,3;fff,90,4,48,3;fff,90,4,70,3;fff,86,3,51,20)
+ "earth" with drop shadow
  + paint it: [local](index.html?ffb,0,100,0,100;1e90ff,5,5,40,50;98fb98,4,5,39,50;1e90ff,10,5,40,10;1e90ff,10,5,60,10;1e90ff,10,10,80,10;1e90ff,15,5,40,30;98fb98,9,10,39,10;98fb98,9,10,59,10;98fb98,9,10,79,10;98fb98,14,5,49,10;1e90ff,24,5,60,30;1e90ff,34,5,40,50;1e90ff,24,10,40,10;1e90ff,29,5,60,10;1e90ff,29,5,80,10;98fb98,23,15,39,10;98fb98,23,15,59,10;98fb98,23,15,79,10;98fb98,23,5,69,10;98fb98,33,5,39,50;1e90ff,43,5,40,50;1e90ff,48,10,40,10;98fb98,42,5,39,50;98fb98,47,10,39,10;1e90ff,67,5,30,60;1e90ff,62,15,40,10;98fb98,66,5,29,60;98fb98,61,15,39,10;1e90ff,81,5,10,80;1e90ff,91,5,40,50;1e90ff,86,5,40,10;98fb98,80,5,9,80;98fb98,90,5,39,50;98fb98,85,5,39,10) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?ffb,0,100,0,100;1e90ff,5,5,40,50;98fb98,4,5,39,50;1e90ff,10,5,40,10;1e90ff,10,5,60,10;1e90ff,10,10,80,10;1e90ff,15,5,40,30;98fb98,9,10,39,10;98fb98,9,10,59,10;98fb98,9,10,79,10;98fb98,14,5,49,10;1e90ff,24,5,60,30;1e90ff,34,5,40,50;1e90ff,24,10,40,10;1e90ff,29,5,60,10;1e90ff,29,5,80,10;98fb98,23,15,39,10;98fb98,23,15,59,10;98fb98,23,15,79,10;98fb98,23,5,69,10;98fb98,33,5,39,50;1e90ff,43,5,40,50;1e90ff,48,10,40,10;98fb98,42,5,39,50;98fb98,47,10,39,10;1e90ff,67,5,30,60;1e90ff,62,15,40,10;98fb98,66,5,29,60;98fb98,61,15,39,10;1e90ff,81,5,10,80;1e90ff,91,5,40,50;1e90ff,86,5,40,10;98fb98,80,5,9,80;98fb98,90,5,39,50;98fb98,85,5,39,10)

### Pictures

+ Piet Mondrian's "Composition II in Red, Blue, and Yellow"
  + [reference @ Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Piet_Mondriaan,_1930_-_Mondrian_Composition_II_in_Red,_Blue,_and_Yellow.jpg)
  + paint it: [local](index.html?f00,33,67,0,67;00f,0,33,67,33;ff0,94,6,86,14;000,32,2,0,100;000,0,100,66,2;000,0,33,31,3;000,93,2,67,33;000,94,6,84,3) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?f00,33,67,0,67;00f,0,33,67,33;ff0,94,6,86,14;000,32,2,0,100;000,0,100,66,2;000,0,33,31,3;000,93,2,67,33;000,94,6,84,3)
+ Red robot
  + [reference @ explodingdog](http://www.explodingdog.com/redrobot.html)
  + paint it: [local](index.html?f00,40,20,30,45;f00,41,5,75,23;f00,54,5,75,23;f00,32,5,34,30;f00,63,5,34,30;f00,32,8,34,8;f00,60,8,34,8;f00,31,2,64,6;f00,36,2,64,6;f00,62,2,64,6;f00,67,2,64,6;f00,49,2,28,2;f00,42,16,8,20;ff0,44,4,12,8;ff0,52,4,12,8) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?f00,40,20,30,45;f00,41,5,75,23;f00,54,5,75,23;f00,32,5,34,30;f00,63,5,34,30;f00,32,8,34,8;f00,60,8,34,8;f00,31,2,64,6;f00,36,2,64,6;f00,62,2,64,6;f00,67,2,64,6;f00,49,2,28,2;f00,42,16,8,20;ff0,44,4,12,8;ff0,52,4,12,8)
+ Mega Man
  + paint it: [local](index.html?0073f7,30,24,9,51;0073f7,54,3,15,6;0073f7,69,9,27,12;0073f7,78,9,33,3;0073f7,6,12,9,12;0073f7,15,9,18,12;0073f7,18,3,15,3;0073f7,12,3,21,3;0073f7,30,6,69,21;0073f7,36,6,66,24;0073f7,54,12,51,12;0073f7,54,18,63,9;0ff,24,42,27,15;0ff,30,24,42,9;0ff,21,3,30,3;0ff,27,3,18,9;0ff,54,9,51,3;0ff,51,9,54,3;0ff,48,9,57,3;0ff,30,6,57,12;0ff,36,6,60,6;ffe7ad,33,24,24,12;ffe7ad,36,6,21,21;f8f8f8,39,9,21,12;f8f8f8,54,3,21,9;f8f8f8,36,3,24,6;f8f8f8,51,3,30,3;050505,27,3,42,33;050505,30,3,75,15;050505,42,3,72,18;050505,33,9,90,3;050505,39,3,63,9;050505,42,12,60,3;050505,54,3,63,6;050505,54,6,69,3;050505,60,12,72,3;050505,69,3,63,3;050505,72,3,66,6;050505,66,3,54,9;050505,63,3,51,3;050505,54,9,48,3;050505,57,9,39,3;050505,54,3,42,6;050505,66,3,30,9;050505,69,12,39,3;050505,69,12,24,3;050505,78,9,27,3;050505,78,9,36,3;0ff,72,15,30,3;050505,87,3,30,6;050505,81,3,30,6;050505,60,9,27,3;050505,57,3,15,18;050505,54,3,33,3;050505,42,9,33,9;050505,39,15,36,3;050505,36,3,39,3;050505,33,3,36,3;050505,30,3,33,3;050505,42,6,24,6;050505,51,3,24,6;050505,48,6,18,3;050505,45,3,15,3;050505,45,12,12,3;0ff,48,6,15,3;0ff,42,9,6,6;050505,51,3,9,3;050505,42,3,9,3;050505,48,3,6,3;050505,39,9,3,3;050505,33,9,6,3;050505,30,3,9,3;050505,27,3,12,6;050505,21,6,18,3;050505,24,3,21,6;050505,27,3,27,6;050505,18,3,12,6;050505,15,3,15,6;050505,15,3,9,3;050505,9,6,6,3;050505,6,3,9,3;050505,3,3,12,6;050505,6,3,18,3;050505,9,3,21,3;050505,12,3,24,3;050505,15,3,27,3;050505,18,3,30,3;050505,21,3,33,3;050505,24,3,36,6) or [@ triskaideka.net](https://triskaideka.net/onekb/blockpainter/?0073f7,30,24,9,51;0073f7,54,3,15,6;0073f7,69,9,27,12;0073f7,78,9,33,3;0073f7,6,12,9,12;0073f7,15,9,18,12;0073f7,18,3,15,3;0073f7,12,3,21,3;0073f7,30,6,69,21;0073f7,36,6,66,24;0073f7,54,12,51,12;0073f7,54,18,63,9;0ff,24,42,27,15;0ff,30,24,42,9;0ff,21,3,30,3;0ff,27,3,18,9;0ff,54,9,51,3;0ff,51,9,54,3;0ff,48,9,57,3;0ff,30,6,57,12;0ff,36,6,60,6;ffe7ad,33,24,24,12;ffe7ad,36,6,21,21;f8f8f8,39,9,21,12;f8f8f8,54,3,21,9;f8f8f8,36,3,24,6;f8f8f8,51,3,30,3;050505,27,3,42,33;050505,30,3,75,15;050505,42,3,72,18;050505,33,9,90,3;050505,39,3,63,9;050505,42,12,60,3;050505,54,3,63,6;050505,54,6,69,3;050505,60,12,72,3;050505,69,3,63,3;050505,72,3,66,6;050505,66,3,54,9;050505,63,3,51,3;050505,54,9,48,3;050505,57,9,39,3;050505,54,3,42,6;050505,66,3,30,9;050505,69,12,39,3;050505,69,12,24,3;050505,78,9,27,3;050505,78,9,36,3;0ff,72,15,30,3;050505,87,3,30,6;050505,81,3,30,6;050505,60,9,27,3;050505,57,3,15,18;050505,54,3,33,3;050505,42,9,33,9;050505,39,15,36,3;050505,36,3,39,3;050505,33,3,36,3;050505,30,3,33,3;050505,42,6,24,6;050505,51,3,24,6;050505,48,6,18,3;050505,45,3,15,3;050505,45,12,12,3;0ff,48,6,15,3;0ff,42,9,6,6;050505,51,3,9,3;050505,42,3,9,3;050505,48,3,6,3;050505,39,9,3,3;050505,33,9,6,3;050505,30,3,9,3;050505,27,3,12,6;050505,21,6,18,3;050505,24,3,21,6;050505,27,3,27,6;050505,18,3,12,6;050505,15,3,15,6;050505,15,3,9,3;050505,9,6,6,3;050505,6,3,9,3;050505,3,3,12,6;050505,6,3,18,3;050505,9,3,21,3;050505,12,3,24,3;050505,15,3,27,3;050505,18,3,30,3;050505,21,3,33,3;050505,24,3,36,6)
